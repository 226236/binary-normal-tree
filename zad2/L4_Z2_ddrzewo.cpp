// plik glowny do implementacji drzewa dowolnego. drzewo takie charakteryzuje sie dowolna liczba
//synow kazdego wezla. dodawac i usuwac mozemy na dowolnych pozycjach
#include <cstdio>
#include <iostream>
#include <string>
#include <list>
#include <cstdlib>
using namespace std;

template<typename typ> //definicja klasy wezla. sklada sie on ze wskaznikow na ojca, tablicy wskaznikow na synow
class Node {           //i kilku metod
public:
	int rozmiar;
	int position;
	int size;
	typ information;
	Node *ojciec;
	Node **synowie;
	Node<typ> get_ojciec();
	bool is_ojciec();
	void set_ojciec(Node<typ> *ojciec);
	typ get_info();
	void set_info(typ information);
	void add_syn(Node<typ> *syn);
	void przesun();
	Node();
	Node(Node<typ> *ojciec, typ information);
};

//konstruktor wezla (inicjuje pusty wezel)
template<typename typ>
Node<typ>::Node() {
	synowie = new Node*[4];
	synowie[0] = NULL;
	synowie[1] = NULL;
	synowie[2] = NULL;
	synowie[3] = NULL;
	size = 4;
	rozmiar = 0;
	position = 0;
	ojciec = NULL;
}

//konstruktor parametryczny dla klasy wezla
template<typename typ>
Node<typ>::Node(Node<typ> *ojciec, typ info)
{
	synowie = new Node*[4];
	synowie[0] = NULL;
	synowie[1] = NULL;
	synowie[2] = NULL;
	synowie[3] = NULL;
	size = 4;
	rozmiar = 0;
	position = 0;
	ojciec = ojciec;
	information = info;
}

//metoda pozwalajaca dostac sie do pola ze wskaznikiem na ojca
template<typename typ>
Node<typ> Node<typ>::get_ojciec() {
	return *ojciec;
}

//sprawdzanie czy ojciec danego wezla jest
template<typename typ>
bool Node<typ>::is_ojciec() {
	if (ojciec == NULL)
	{
		return false;
	}
	else {
		return true;
	}
}

//ustawienie ojca na konkretna wartosc
template<typename typ>
void Node<typ>::set_ojciec(Node<typ> *ojciec) {
	ojciec = ojciec;
}

//przesuniecie wskaznika przy przeszukiwaniu
template<typename typ>
void Node<typ>::przesun() {
	position = position + 1;
}

//metoda wyciagajaca informacje o wartosci danego wezla
template<typename typ>
typ Node<typ>::get_info() {
	return information;
}

//przypisanie wartosci do wezla
template<typename typ>
void Node<typ>::set_info(typ info) {
	information = info;
}

//dodanie syna do wezla
template<typename typ>
void Node<typ>::add_syn(Node<typ> *cos) {

	int rozmiar = this->rozmiar;
	if (rozmiar < size-2)
	{
		this->synowie[rozmiar] = cos;
		this->rozmiar = this->rozmiar + 1;
		rozmiar = rozmiar + 1;
	}
	else
	{
		int prt = size * 2;
		Node <typ> **tab = new Node<typ>*[prt];
		for (int i = 0; i < rozmiar; i++)
		{
			tab[i] = this->synowie[i];
		}
		this->synowie = tab;
		this->synowie[rozmiar] = cos;
		this->size = size * 2;
		this->rozmiar = this->rozmiar + 1;
		rozmiar = this->rozmiar;
		for (int i = rozmiar; i < size; i++)
		{
			this->synowie[i] = NULL;
		}
	}
}

//definicja klasy drzewa. w metodach zdefiniowane sa sposoby przechodzenia drzewa
template<typename typ>
class Tree {
private:
	Node<typ> *root;
public:
	void Pre_order(Node<typ> *wezel);
	void Post_order(Node<typ> *wezel);
	void In_order(Node<typ> *wezel);
	int korzen();
	void usun(typ usuwam, Node<typ>* wezel);
	int wysokosc(int licznik, Node<typ> *wezel);
	void dodaj_wezel(Node<typ> *temp, typ dane);
	Tree();
	Tree(Node<typ> *root);
};

//konstruktoe
template<typename typ>
Tree<typ>::Tree() {
	root = NULL;
}

//
template<typename typ>
int Tree<typ>::korzen() {
	return root->rozmiar;
}

//zwraca wysokosc drzewa
template<typename typ>
int Tree<typ>::wysokosc(int licznik, Node<typ>* wezel)
{
	int h = licznik;
	int	rozmiar = wezel->rozmiar;
	Node<typ> *temp = new Node<typ>();
	for (int i = 0; i < rozmiar; i++)
	{
		temp = wezel->synowie[i];
		h++;
		wysokosc(h, temp);
	}
	return h;
}

//konstruktor parametryczny drzewa
template<typename typ>
Tree<typ>::Tree(Node<typ> *nowy)
{
	root = nowy;
}

//przechodzenie pre order
template<typename typ>
void Tree<typ>::Pre_order(Node<typ> *wezel)
{
	int	rozmiar = wezel->rozmiar;
	cout << wezel->information << " ";
	Node<typ> *temp = new Node<typ>();
	for (int i = 0; i < rozmiar; i++)
	{
		temp = wezel->synowie[i];
		Pre_order(temp);
	}
}

//przechodzenie post order
template<typename typ>
void Tree<typ>::Post_order(Node<typ> *wezel)
{
	int	rozmiar = wezel->rozmiar;
	Node<typ> *temp = new Node<typ>();
	for (int i = 0; i < rozmiar; i++)
	{
		temp = wezel->synowie[i];
		Post_order(temp);
	}
	cout << wezel->information << " ";
}

//przechodzenie in order
template<typename typ>
void Tree<typ>::In_order(Node<typ> *wezel)
{
	if (wezel->synowie[0] != NULL)
		In_order(wezel->synowie[0]);
	cout << wezel->information << " ";
	for (int i = 1; i<wezel->rozmiar; i++)
		In_order(wezel->synowie[i]);
}

//usuwanie elementu z drzewa
template<typename typ>
void Tree<typ>::usun(typ usuwam,Node<typ> *wezel)
{
	Node<typ> *temp = new Node<typ>();
	for (int i = 0; i < wezel->rozmiar; i++)
	{
		if (wezel->synowie[i]->information != usuwam)
		{
			temp = wezel->synowie[i];
			usun(usuwam, temp);
		}
		else
		{
			wezel->synowie[i] = NULL;
			Node<typ> **pomoc = new Node<typ>*[wezel->size];
			int j = 0;
			for (int i = 0; i < wezel->size; i++)
			{
				if (j < wezel->size) {
					if (wezel->synowie[j] != NULL)
					{
						pomoc[i] = wezel->synowie[j];
						j++;
					}
					else
					{
						j++;
						pomoc[i] = wezel->synowie[j];
						j++;
					}
				}
			}
			wezel->synowie = pomoc;
			wezel->rozmiar = wezel->rozmiar - 1;
			for (int i = wezel->rozmiar; i < wezel->size; i++)
			{
				wezel->synowie[i] = NULL;
			}
		}
	}
}

//dodawanie wezla
template<typename typ>
void Tree<typ>::dodaj_wezel(Node<typ>* temp, typ dane)
{
	if (root == NULL)
	{
		Node<typ> *n1 = new Node<typ>(NULL, dane);
		root = n1;
	}
	else
	{
		int trp = temp->rozmiar;
		trp = trp +1;
		int zmienna = rand() % trp;
		if (temp->synowie[zmienna] == NULL)
		{
			Node<typ> *n1 = new Node<typ>(temp, dane);
			temp->add_syn(n1);
		}
		else
		{
			temp = temp->synowie[zmienna];
			dodaj_wezel(temp, dane);
		}
	}
}

//funkcja glowna. nie zawiera menu tylko prezentuje wszystkie funkcje drzewa
int main()
{
	Node<int> *wezel = new Node<int>(NULL, 5);
	int dane;
	Tree<int> *drzewo = new Tree<int>(wezel);

	dane = 1;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 2;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 3;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 4;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 5;
	drzewo->dodaj_wezel(wezel, dane);
	dane = 6;
	drzewo->dodaj_wezel(wezel, dane);
	cout << "Post" << endl;
	cout << "Wysokosc: ";
	cout<< drzewo->wysokosc(0, wezel) <<endl;
	drzewo->Post_order(wezel);
	cout << endl;
	cout << "Pre" << endl;
	drzewo->Pre_order(wezel);
	cout << endl;
	cout << "In" << endl;
	drzewo->In_order(wezel);
	cout << endl;
	drzewo->usun(4, wezel);
	cout<<"Post" << endl;
	cout << "Wysokosc: ";
	cout<<drzewo->wysokosc(0, wezel)<<endl;
	drzewo->Post_order(wezel);
	cout << endl;
	cout << "Pre" << endl;
	drzewo->Pre_order(wezel);
	cout << endl;
	cout << "In" << endl;
	drzewo->In_order(wezel);
	cout << endl;
	return 0;
}
