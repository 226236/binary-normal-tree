#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

#define PRINT_TREE

/* Definicja klasy reprezentujacej klucz */
class klucz {
      public:
              /* Zmienna przechowujaca wskaznik na lewego potomka */
              klucz *left;
              /* Zmienna przechowujaca wskaznik na prawego potomka */
              klucz *right;
              /* Zmienna przechowujaca wskaznik na rodzica */
              klucz *parent;
              /* Zmienna przechowujaca wartosc klucza */
              int value;
      };

/* Definicja klasy reprezentujacej binarne drzewo poszukiwan */
class BST {
      private:
              /* Zmienna bedaca wskaznikiem na korzen drzewa */
              klucz *root;
              /* Zmienna przechowujaca rozmiar drzewa */
              int size;
      public:
             /* Prototyp konstruktora */
             BST();
             /* Prototyp destruktora */
             ~BST();
             /* Prototyp funkcji dodajacej klucz do drzewa */
             int Dodaj(const int a);
             /* Prototyp funkcji szukajacej klucza w drzewie */
             klucz *Szukaj(const int tmp);
             /* Prototyp funkcji usuwajacej dany klucz */
             klucz *Usun(klucz * usuwany);
             /* Prototyp funkcji wyszukujacej klucz o wartosci min */
             klucz *Minimum(klucz *x);
             klucz *Nastepnik(klucz *x);
             /* Prototyp funkcji wyswietlajacej drzewo */
             void WyswietlDrzewo(ostream &s,const klucz *tmp)const;
             /* Prototyp funkcji usuwajacej dany klucz */
             void Usunklucz(klucz *tmp);
             /* Przeciazenie operatora wyjscia */
             ostream &WyswietlDrzewo(ostream &s)const
             { if(root) WyswietlDrzewo(s,root); return s; }
      };

/* Przeciazenie operatora wyjscia */
ostream &operator<<(ostream &s,const BST &d) { return d.WyswietlDrzewo(s); }

/* Definicja konstruktora drzewa */
BST:: BST()
{
         root = NULL;
         size = 0;
}

/* Definicja destruktora drzewa */
BST:: ~BST()
{
         Usunklucz(root);
}

/* Definicja funkcji usuwajacej dany klucz */
void BST::Usunklucz(klucz *tmp)
  {
     if(tmp->left)  Usunklucz(tmp->left);
     if(tmp->right) Usunklucz(tmp->right);
     delete tmp;
  }

/* Definicja funkcji dodajacej nowy klucz */


int BST:: Dodaj(const int a)
{
     /* Stworzenie nowego wezla i przypisanie argumentu funkcji */
     /* jako wartosci klucza */
     klucz *nowy;
     nowy = new klucz;
     nowy -> value = a;
     nowy -> left = NULL;
     nowy -> right = NULL;
     nowy -> parent = NULL;

     /* Przypadek gdy BST jest puste */
     if (size == 0)
     {
        root = nowy;
        size++;
        return 0;
     }

     /* W innym wypadku nastepuje przeszukanie drzewa */
     if (size != 0)
     {   /* Utworzenie tymczasowego wskaznika na korzen */
         klucz *miejsce = root;
         /* Utworzenie tymczasowego wskaznika na rodzica poszukiwanego klucza */
         klucz *ojciecmiejsca=NULL;
         //petla wykonuje sie dopoki miejsce nie jest wskaznikiem na zero
         while(miejsce != NULL)
         {
               /* Stary klucz staje sie rodzicem */
               ojciecmiejsca = miejsce;
               /* Zmienna pomocnicza */
               int _value = (miejsce -> value);
               /* Przypadek gdy wartosc jest mniejsza = idziemy na lewo */
               if(a <  _value)
               {
                    miejsce = miejsce->left;
               }
               /* Przypadek gdy wartosc jest wieksza = idziemy na prawo */
               if (a > _value)
               {
                     miejsce = miejsce ->right;
               }
               /* Przypadek, gdy znajdziemy rowna wartosc=konczymy dzialanie */
               if(a == _value)
               {
                  delete nowy;
                  return 1;
               }
         }
         /* Korzystamy z rodzica miejsca znalezionego w poprzednim while'u */
         /* Przypadek gdy wartosc jest mniejsza od klucza - idziemy na lewo */
         if ( a < ojciecmiejsca->value)
         {
              /* Dodajemy nowy klucz */
              ojciecmiejsca->left = nowy;
              nowy -> parent = ojciecmiejsca;
              size++;
              return 0;
         }
         /* Przypadek gdy wartosc jest mniejsza od klucza - idziemy na prawo */
         if ( a > ojciecmiejsca->value)
         {
              /* Dodajemy nowy klucz */
              ojciecmiejsca->right = nowy;
              nowy -> parent = ojciecmiejsca;
              size++;
              return 0;
         }
     }
return 0;
}


/* Definicja funkcji wyszukujacej dana wartosc w BST */
/* Funkcja zwraca wskaznik na klucz przechowujacy dana wartosc */


klucz* BST:: Szukaj(const int tmp)
{
     /* Deklaracja tymczasowego wskaznika */
     /* Rozpoczyna swoje szukanie w korzeniu */
     klucz *miejsce = root;
     while(miejsce!=NULL && tmp!=miejsce->value)
     {
          if (tmp < miejsce->value)
          {
                miejsce = miejsce->left;
          }
          if (tmp > miejsce->value)
          {
                miejsce = miejsce->right;
          }
     }
     return miejsce;
}

/* Definicja funkcji znajdujacej minimum od danego klucza */
/* Przechodzi do liscia najbardziej na lewo */
klucz *BST:: Minimum(klucz *x)
{
      klucz* minimum = x;
      while(minimum->left)
      {
             minimum = minimum->left;
      }
      return minimum;
}

/* Definicja funkcji zwracajacej wskaznik na nastepnik danego wezla    */
/* Nastepnik -  jezeli wezel x posiada prawego potomka, to nastepnikiem */
/* jest wezel minimalny w prawym poddrzewie BST wezla x. Jesli wezel x */
/* nie posiada prawego potomka, to nastepnikiem bedzie pierwszy rodzic,*/
/* dla którego wezel x lezy w lewym poddrzewie                         */
klucz *BST::Nastepnik(klucz *x)
{
    if(x->right)
    {
         return Minimum(x->right);
    }
    klucz *tmp;
    do
    {
         tmp = x;
         x = x->parent;
    } while (x && (x->left != tmp));
    return x;
}


klucz *BST:: Usun(klucz * usuwany)
{
    klucz *rodzic_usuwanego;
    rodzic_usuwanego = usuwany->parent;

    klucz *z;
    if((usuwany->left) && (usuwany->right))
    {
       z =  Usun( Nastepnik(usuwany) );
       z->left = usuwany->left;
       if(z->left)   z->left->parent = z;
       z->right = usuwany->right;
       if(z->right)  z->right->parent = z;
    }
    else z = (usuwany->left) ? usuwany->left : usuwany->right;
    if(z) z->parent = rodzic_usuwanego;
    if(!rodzic_usuwanego) root = z;
    else if(rodzic_usuwanego->left == usuwany) rodzic_usuwanego->left = z;
    else rodzic_usuwanego->right = z;
    return usuwany;
}

/* Definicja funkcji wyswietlajacej drzewo */
void BST::WyswietlDrzewo(ostream &s,const klucz *tmp)const
{
    if(tmp->left) WyswietlDrzewo(s,tmp->left);
    #ifdef PRINT_TREE
    int value=tmp->value;
    const klucz *p=root,*c;
    for(int pv=0,v=1;p;p=c,pv=v)
     {
      int pvalue=p->value;
      if(value<pvalue)      { v=-1; c=p->left;  }
      else if(value>pvalue) { v=+1; c=p->right; }
      else                      { v=0;  c=0;        }
      if(p!=root) s<<(v?(v!=pv?"\xB3  ":"   "):
                  (pv<0?"\xDA\xC4\xC4":"\xC0\xC4\xC4"));
     }
    s<<'\xDB'<<' '<<value<<endl;
    #else
    s<<tmp->value<<' ';
    #endif
    if(tmp->right) WyswietlDrzewo(s,tmp->right);
}

/****************************************************************************/


int main()
{
    srand(time(NULL));
    BST tree;
    klucz *znajdz, *usun;
    int spr,a,i;
    char tmp;
    for(i=0;i<10;i++)
    {

        a=rand()%100;
        tree.Dodaj(a);
    }
    cout<<endl<<tree<<endl;
    for(i=9;i>=0;i--)
    {
        znajdz=tree.Szukaj(i);
        usun=tree.Usun(znajdz);
        delete usun;
    }
    return 0;
}
