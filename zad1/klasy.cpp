//plik z definicjami metod
#include <iostream>
#include "klasy.hh"
using namespace std;

pqueue::pqueue()
{
  poczatek=koniec=NULL;
}

pqueue::~pqueue()
{
  while(poczatek)
    {
      pop();
    }
}

void pqueue::push(int wartosc, int priorytet)
{
  element *p, *r; //tworzymy nowe, pomocniczne elementy kolejki
  p= new element;
  p->next=NULL;
  p->wart=wartosc;
  p->prio=priorytet;
  if(!poczatek) //sprawdzamy czy kolejka jest pusta
    {
      poczatek=koniec=p; //jesli tak, dodawany element staje sie pierwszym 
    }                    //czyli jednoczesnie poczatkiem i koncem
  else
    {
      if(poczatek->prio<priorytet) //w innym przypadku segregujemy wg priorytetu
	{
	  p->next=poczatek;
	  poczatek=p;
	}
      else
	{
	  r=poczatek;
	  while((r->next) && (r->next->prio=priorytet))
	    {
	      r=r->next;
	    }
	  p->next=r->next;
	  r->next=p;
	  if(!p->next)
	    {
	      koniec=p;
	    }
	}
    }
}

bool pqueue::empty(void)
{
  return !poczatek;
}

int pqueue::front(void)
{
  if(poczatek)
    {
      return poczatek->wart;
    }
  else
    {
      cout <<"Kolejka pusta!"<<endl;
      return 0;
    }
}

int pqueue::frontprio(void)
{
  if(poczatek)
    {
      return poczatek->prio;
    }
  else
    {
      cout<<"Kolejka pusta!"<<endl;
      return 0;
    }
}
void pqueue::pop(void)
{
  if(poczatek)
    {
      element *p=poczatek;
      poczatek=poczatek->next;
      if(!poczatek)
	{
	  koniec=NULL;
	}
    }
}
