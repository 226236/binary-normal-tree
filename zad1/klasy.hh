//plik z opisem klasy i prototypami metod

#ifndef KLASY_HH
#define KLASY_HH

struct element
{
  int wart;
  int prio; //priorytet danego elementu
  element *next; //wskaznik na nastepny element
};

class pqueue
{
private:
  element *poczatek;
  element *koniec;
public:
  pqueue();
  ~pqueue();
  bool empty(void);
  void pop(void);
  int front(void);
  int frontprio(void);
  void push(int wartosc, int priorytet);
};

#endif
